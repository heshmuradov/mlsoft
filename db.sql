--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

--
-- Name: parts_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE parts_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.parts_id_seq OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: parts; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE parts (
    id bigint DEFAULT nextval('parts_id_seq'::regclass) NOT NULL,
    partname character varying(100),
    partnum character varying(100),
    vendor character varying(100),
    qty integer,
    shipped date,
    received date
);


ALTER TABLE public.parts OWNER TO postgres;

--
-- Data for Name: parts; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO parts VALUES (6, 'Part Kim', '22', 'South-Hall', 45000, '2017-10-01', '2017-12-01');
INSERT INTO parts VALUES (7, 'Part James', '24', 'Houston', 10000, NULL, NULL);
INSERT INTO parts VALUES (9, 'Part 1', '33', 'myVendor', 123, '2017-10-20', '2017-12-20');
INSERT INTO parts VALUES (5, 'Part David', '27', 'Texas', 85000, '2017-10-12', '2017-12-11');
INSERT INTO parts VALUES (3, 'Part Blade7', '23', 'Norway', 20000, '2017-10-10', '2017-12-31');
INSERT INTO parts VALUES (4, 'Mark Part', '25', 'Rich-Mond ', 65000, '2017-10-10', NULL);


--
-- Name: parts_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('parts_id_seq', 1, false);


--
-- Name: pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY parts
    ADD CONSTRAINT pkey PRIMARY KEY (id);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--