package com.sample;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.HashMap;

public class PartDAO {
	private String jdbcURL;
	private String jdbcUsername;
	private String jdbcPassword;
	private Connection jdbcConnection;
	
	public PartDAO(String jdbcURL, String jdbcUsername, String jdbcPassword) {
            this.jdbcURL = jdbcURL;
            this.jdbcUsername = jdbcUsername;
            this.jdbcPassword = jdbcPassword;
	}
	
	protected void connect() throws SQLException {
            if (jdbcConnection == null || jdbcConnection.isClosed()) {
                try {
                        Class.forName("org.postgresql.Driver");
                } catch (ClassNotFoundException e) {
                        throw new SQLException(e);
                }
                jdbcConnection = DriverManager.getConnection(jdbcURL, jdbcUsername, jdbcPassword);
            }
	}
	
	protected void disconnect() throws SQLException {
            if (jdbcConnection != null && !jdbcConnection.isClosed()) {
                jdbcConnection.close();
            }
	}
	
	public List<Part> listParts(HashMap<String,String> filters, String sortBy, String sortType) {
            List<Part> listPart = new ArrayList<>();
            ArrayList<String> params = new ArrayList<>();
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT * FROM parts");
            if (!filters.isEmpty()) {
                sql.append(" WHERE 1=1");
                if (filters.containsKey("partname")) {
                    sql.append(" AND partname like ?");
                    params.add("%"+filters.get("partname")+"%");
                }
                if (filters.containsKey("partnum")) {
                    sql.append(" AND partnum like ?");
                    params.add("%"+filters.get("partnum")+"%");
                }
                if (filters.containsKey("vendor")) {
                    sql.append(" AND vendor like ?");
                    params.add("%"+filters.get("vendor")+"%");
                }
                if (filters.containsKey("qty")) {
                    sql.append(" AND qty >= ? ::integer");
                    params.add(filters.get("qty"));
                }
                if (filters.containsKey("shipped1")) {
                    sql.append(" AND shipped > ? ::date");
                    params.add(filters.get("shipped1"));
                }
                if (filters.containsKey("shipped2")) {
                    sql.append(" AND shipped < ? ::date");
                    params.add(filters.get("shipped2"));
                }
                if (filters.containsKey("received1")) {
                    sql.append(" AND received > ? ::date");
                    params.add(filters.get("received1"));
                }
                if (filters.containsKey("received2")) {
                    sql.append(" AND received < ? ::date");
                    params.add(filters.get("received2"));
                }
            }
            if (sortBy != null) {
                sql.append(" ORDER BY ").append(sortBy).append(" ").append(sortType);
            }
//            System.out.println(sql.toString());

            PreparedStatement statement = null;
            try {
                connect();
                statement = jdbcConnection.prepareStatement(sql.toString());
                for (int i = 0; i < params.size(); i++) {
                    //System.out.println(params.get(i));
                    statement.setString(i+1, params.get(i));
                }
        		ResultSet resultSet = statement.executeQuery();
        		while (resultSet.next()) {
                            Part part = new Part();
                            part.setName(resultSet.getString("partname"));
                            part.setNumber(resultSet.getString("partnum"));
                            part.setQty(resultSet.getInt("qty"));
                            part.setVendor(resultSet.getString("vendor"));
                            part.setShipped(resultSet.getDate("shipped"));
                            part.setReceived(resultSet.getDate("received"));
                            listPart.add(part);
        		}
                resultSet.close();
		
            } catch (SQLException e) {
                 e.printStackTrace();
            } finally {
                 try {
                     if (statement != null) {
                        statement.close();
                     }
                     disconnect();
                 } catch (SQLException e) {
                     e.printStackTrace();
                 }
            }		

            return listPart;
        }
		
}
