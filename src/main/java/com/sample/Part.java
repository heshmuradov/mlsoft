package com.sample;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * This is a model class represents a part entity
 *
 */
public class Part {
    protected Long id;
    protected String name;
    protected String number;
    protected String vendor;
    protected Integer qty;
    protected Date shipped;
    protected Date received;

    public Part() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getVendor() {
        return vendor;
    }

    public void setVendor(String vendor) {
        this.vendor = vendor;
    }

    public Integer getQty() {
        return qty;
    }

    public void setQty(Integer qty) {
        this.qty = qty;
    }

    public Date getShipped() {
        return shipped;
    }

    public void setShipped(Date shipped) {
        this.shipped = shipped;
    }

    public Date getReceived() {
        return received;
    }

    public void setReceived(Date received) {
        this.received = received;
    }

    public String formatShipped(String format) {
        SimpleDateFormat df = new SimpleDateFormat(format);        
        String fshipped = (shipped!=null) ? df.format(shipped) : "";
        return fshipped;
    }

    public String formatReceived(String format) {
        SimpleDateFormat df = new SimpleDateFormat(format);        
        String freceived = (received!=null) ? df.format(received) : "";
        return freceived;
    }

}
