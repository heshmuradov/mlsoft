package com.sample;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * PartServlet.java
 */
public class PartServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private PartDAO partDAO;

	public void init() {
		String jdbcURL = getServletContext().getInitParameter("jdbcURL");
		String jdbcUsername = getServletContext().getInitParameter("jdbcUsername");
		String jdbcPassword = getServletContext().getInitParameter("jdbcPassword");

		partDAO = new PartDAO(jdbcURL, jdbcUsername, jdbcPassword);

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
            // store filter in session
            HashMap<String,String> filters = new HashMap<>();
            Enumeration<String> e = request.getParameterNames();
            while (e.hasMoreElements()) {
                String name = e.nextElement();
                String value = request.getParameter(name);
                if (!value.trim().isEmpty()) {
                    filters.put(name, value);
                }
            }
            HttpSession session = request.getSession(true);
            session.setAttribute("filters", filters);
            
            doGet(request, response);
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

            HashMap<String,String> filters;
            HttpSession session = request.getSession(true);
            if (session.getAttribute("filters") != null) {
                filters = (HashMap)session.getAttribute("filters");
            }
            else {
                filters = new HashMap<>();
            }
            

            String asc = request.getParameter("asc");
            String desc = request.getParameter("desc");
            
            Properties pr = updateSortingProperties(asc, desc);
            
            String sortBy   = (asc != null) ? asc : desc;
            String sortType = (asc != null) ? "asc" : "desc";
            List<Part> parts = partDAO.listParts(filters, sortBy, sortType);
            
            response.setContentType("text/html");
            PrintWriter out = response.getWriter();
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Sample</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h3>Parts Web Application</h3>");
            out.println("<form method=\"post\"> "); 
            out.println("<table width=\"100%\">"); 
            out.println("<tr bgcolor=\"lightgrey\" align=\"left\">"); 
            out.println("        <th colspan=2 width=\"100px\">Filter</th>"); 
            out.println("</tr>"); 
            out.println("<tr>"); 
            out.println("        <td>PN</td>"); 
            out.println("        <td><input type=\"text\" name=\"partnum\" value=\""+filters.getOrDefault("partnum","")+"\"/></td>	"); 
            out.println("</tr>"); 
            out.println("<tr>"); 
            out.println("        <td>Part Name</td>"); 
            out.println("        <td><input type=\"text\" name=\"partname\" value=\""+filters.getOrDefault("partname","")+"\"/></td>	"); 
            out.println("</tr>"); 
            out.println("<tr>"); 
            out.println("        <td>Vendor</td>"); 
            out.println("        <td><input type=\"text\" name=\"vendor\" value=\""+filters.getOrDefault("vendor","")+"\"/></td>	"); 
            out.println("</tr>"); 
            out.println("<tr>"); 
            out.println("        <td>Quantity</td>"); 
            out.println("        <td><input size=\"5\" type=\"text\" name=\"qty\" value=\""+filters.getOrDefault("qty","")+"\"/></td>	"); 
            out.println("</tr>"); 
            out.println("<tr>"); 
            out.println("        <td>Shipped</td>"); 
            out.println("        <td>after<input type=\"text\" name=\"shipped1\" value=\""+filters.getOrDefault("shipped1","")+"\"/> before<input type=\"text\" name=\"shipped2\" value=\""+filters.getOrDefault("shipped2","")+"\"/></td>	"); 
            out.println("</tr>"); 
            out.println("<tr>"); 
            out.println("        <td>Received</td>"); 
            out.println("        <td>after<input type=\"text\" name=\"received1\" value=\""+filters.getOrDefault("received1","")+"\"/> before<input type=\"text\" name=\"received2\" value=\""+filters.getOrDefault("received2","")+"\"/></td>	"); 
            out.println("</tr>"); 
            out.println("</table>"); 
            out.println("<br>"); 
            out.println("<input type=\"submit\" value=\"Filter\"/>"); 
            out.println("</form>"); 
            out.println("<br>"); 
            out.println("<table width=\"100%\">"); 
            out.println("<tr bgcolor=\"lightgrey\" align=\"left\">"); 
            out.println("<th width=\"20%\"><a href=\"?".concat(pr.getProperty("partnum")).concat("=").concat("partnum").concat("\">PN</a></th>"));
            out.println("<th width=\"30%\"><a href=\"?".concat(pr.getProperty("partname")).concat("=").concat("partname").concat("\">Part Name</a></th>")); 
            out.println("<th width=\"20%\"><a href=\"?".concat(pr.getProperty("vendor")).concat("=").concat("vendor").concat("\">Vendor</a></th>")); 
            out.println("<th><a href=\"?".concat(pr.getProperty("qty")).concat("=").concat("qty").concat("\">Qty</a></th>")); 
            out.println("<th><a href=\"?".concat(pr.getProperty("shipped")).concat("=").concat("shipped").concat("\">Shipped</a></th>")); 
            out.println("<th><a href=\"?".concat(pr.getProperty("received")).concat("=").concat("received").concat("\">Received</a></th>")); 
            out.println("</tr>");
            
            for (int i = 0; i < parts.size(); i++) {
                Part part = parts.get(i);
                out.println("<tr>");
                out.println("<td>"+part.number+"</td>");
                out.println("<td>"+part.name+"</td>");
                out.println("<td>"+part.vendor+"</td>");
                out.println("<td>"+part.qty+"</td>");
                out.println("<td>"+part.formatShipped("MMM dd, yyyy")+"</td>");
                out.println("<td>"+part.formatReceived("MMM dd, yyyy")+"</td>");
                out.println("</tr>");
            }
            out.println("</table>");
            out.println("</body>");
            out.println("</html>");

	}

          
        private Properties updateSortingProperties(String asc, String desc) {
            Properties pr = new Properties();
            pr.setProperty("partnum","asc");
            pr.setProperty("partname","asc");
            pr.setProperty("vendor","asc");
            pr.setProperty("qty","asc");
            pr.setProperty("shipped","asc");
            pr.setProperty("received","asc");

            if (asc != null) {
                pr.setProperty(asc, "desc");
            }
            if (desc != null) {
                pr.setProperty(desc, "asc");
            }
            return pr;
        }


}
